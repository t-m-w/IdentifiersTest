package com.example.identifierstest

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import java.io.BufferedReader
import java.io.File
import java.nio.file.Files
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.security.MessageDigest
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

class IdentifierUtils(context : Context) {
    companion object {
        private const val ANDROID_USER_DIVISOR = 100000
        private const val ANDROID_APP_INSTALL_DIR = "/data/app"

        private val preciseDateTimeFormatter = DateTimeFormatter.ofPattern(
            "uuuu-MM-dd HH:mm:ss.nnnnnnnnn"
        ).withZone(ZoneOffset.UTC)

        @JvmStatic
        private fun getParentDir(path : String) : String {
            return File(path).parent!!
        }

        @JvmStatic
        private fun formatDateTimePrecisely(date : TemporalAccessor) : String? {
            return preciseDateTimeFormatter.format(date)
        }

        fun TemporalAccessor.toStringPrecise() : String? {
            return try {
                preciseDateTimeFormatter.format(this)
            } catch (e: Exception) {
                null
            }
        }

        fun getShortSha1(bytes : ByteArray) : String {
            return try {
                MessageDigest.getInstance("SHA-1").digest(bytes)
                    .take(4).joinToString(""){ "%02x".format(it) }
            } catch (e : Exception) {
                "<failed>"
            }
        }
    }


    private val _context : Context = context

    fun getUserId(uid : Int = _context.applicationInfo.uid) : Int {
        return uid / ANDROID_USER_DIVISOR
    }

    fun guessAndroidInstallTime() : String? {
        return guessAndroidInstallTimeViaStat() ?:
            guessAndroidInstallTimeViaJava()?.toInstant()?.toStringPrecise()
    }

    fun guessAndroidInstallTimeViaStat() : String? {
        val proc = Runtime.getRuntime().exec("stat -c %x $ANDROID_APP_INSTALL_DIR")
        proc.waitFor()
        val allText = proc.inputStream.bufferedReader().use(BufferedReader::readText)
        return allText.trim().ifEmpty { null }
    }

    fun guessAndroidInstallTimeViaJava() : FileTime? {
        return try {
            Files.readAttributes(
                File(ANDROID_APP_INSTALL_DIR).toPath(),
                BasicFileAttributes::class.java
            ).lastAccessTime()
        } catch (e : Exception) {
            null
        }
    }

    fun getAppFingerprint(app : ApplicationInfo) : String {
        return getShortSha1(getParentDir(app.publicSourceDir).toByteArray())
    }

    fun outputAppIdentifiers(app : ApplicationInfo) : String {
        return "${getAppFingerprint(app)} " +
                "${app.packageName}\n" +
                "${app.uid} ${getParentDir(app.publicSourceDir)}\n"
    }

    fun outputInfoSelf() : String {
        val app = _context.applicationInfo
        return _context.getString(R.string.fingerprint_line, getAppFingerprint(app)) + "\n\n" +
                "${outputAppIdentifiers(app)}\n" +
                _context.getString(R.string.other_info,
                    guessAndroidInstallTime() ?: "<unknown>",
                    getUserId(),
                    app.uid - (ANDROID_USER_DIVISOR * getUserId())
                )
    }

    fun outputInfoOthers() : String {
        val pkgs = _context.packageManager.getInstalledPackages(
            PackageManager.MATCH_UNINSTALLED_PACKAGES)
        return pkgs.filter {
                    it.applicationInfo.publicSourceDir.startsWith(ANDROID_APP_INSTALL_DIR)
                }.joinToString("\n") {
                    outputAppIdentifiers(it.applicationInfo)
                }.trim()
    }

    fun outputInfoAbout() : String {
        return "${_context.getString(R.string.pkg_identifier_format_title)}:\n" +
                "${_context.getString(R.string.pkg_identifier_format_description)}\n\n" +
                "${_context.getString(R.string.app_fingerprint_explainer_1)}\n\n" +
                "${_context.getString(R.string.app_fingerprint_explainer_2)}\n\n" +
                "${_context.getString(R.string.app_fingerprint_explainer_3)}\n\n" +
                _context.getString(R.string.android_install_time_explainer)
    }
}