package com.example.identifierstest.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PageViewModel : ViewModel() {

    private val _body = MutableLiveData<String>()
    private val _horizontalScrolling = MutableLiveData<Boolean>()
    val text: LiveData<String> = Transformations.map(_body) { it }
    val horizontalScrolling: LiveData<Boolean> = Transformations.map(_horizontalScrolling) { it }

    fun setBody(body: String, horizontalScrolling: Boolean = false) {
        _body.value = body
        _horizontalScrolling.value = horizontalScrolling
    }
}